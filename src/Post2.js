import React from 'react';
import './Post2.css';

const Post2 = (props) => {
    return (
        <p className="post2">{props.text}</p>
    )
};

export default Post2;

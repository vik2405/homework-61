import React, { Component } from 'react';
import './App.css';
import Post from "./Post";
import Post2 from "./Post2";

class App extends Component {

    state = {
        post: [],
        country: {},
        borders: []
    };
    getPost = () => {
        fetch('https://restcountries.eu/rest/v2/all?fields=name;borders;capital').then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        }).then(info => {
            console.log(info);
            this.setState({post: info});
        })
    };
    componentDidMount() {
        this.getPost();
    }

    findCountry = (name) => {
        let countrys = this.state.post;
        let index = countrys.findIndex(country => country.name === name );
        let country = countrys[index];
        let countryInfo = {
            name: country.name,
            capital: country.capital
        };

        let promises = country.borders.map(code => {
            return fetch('https://restcountries.eu/rest/v2/alpha/' + code + '?fields=name').then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong');
            });
        });
        Promise.all(promises).then(info2 => {
           this.setState({country: countryInfo, borders: info2});
        })
    };

    render() {
    return (
      <div className="App">
          <div className="a">
              <div className="title">SELECT COUNTRYS NAME</div>
          {this.state.post.map(item => <Post key={item.name} text={item.name} click={() => this.findCountry(item.name)}/>)}
          </div>
          <div className="b">
              <h4>COUNTRY NAME</h4>
              {this.state.country.name}
              <h4>COUNTRY CAPITAL</h4>
              {this.state.country.capital}
          </div>
          <div className="b">
              <h4> ADJOINING COUNTRY</h4>
          {this.state.borders.map(item => <Post2 key={item.name} text={item.name} />)}
          </div>
      </div>
    );
  }
}

export default App;

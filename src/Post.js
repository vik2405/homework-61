import React from 'react';
import './Post.css';
const Post = (props) => {
    return (
        <p className="post" onClick={props.click}>{props.text}</p>
    )
};

export default Post;